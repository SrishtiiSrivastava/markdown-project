## Lab 1: Create an instance of Watson Assistant

In this course, we'll use the Watson Assistant service hosted on the IBM Cloud platform. As a
result, before we progress further, we'll need to ensure that you are set up with an account.
Follow the instructions below to get your own instance of Watson Assistant and start building
chatbots.

```
Note: If you already have an IBM Cloud account, simply login and skip to Exercise 2.
```
# Exercise 1: Register with IBM Cloud

1. **Register** on IBM Cloud by [clicking on this link](https://cocl.us/CB0103EN_BM). Start by entering your professional or student
    email. (If you don't have access to a business email, feel free to use your personal email,
    however.)
2. You'll be asked to log in if you already have an account with IBM.com. If you do, log in and
    skip to Exercise 2. If you don't, provide the rest of the information requested (e.g., your
    name, country, etc.) and **click on** **_Create an account_** to proceed with the creation of an
    account.
3. If you created an account in step 2, you'll be asked to **verify your email**. Check your inbox
    for the verification email (and for good measure, be sure to check your spam folder as well).
    **Click on the confirmation button** as illustrated in the image below.

<img src="https://gitlab.com/SrishtiiSrivastava/markdown-project/-/raw/master/images/1.1.PNG"  width = 600>

4. Upon successful verification of your email, you'll see a _Log In_ button. **Click on it, log in, and**
    **accept the privacy notices**. At this point, you'll be logged in with your IBM ID and ready to
    work with the IBM Cloud platform.

You’ll either be redirected to a Watson Assistant creation page (as shown in the image
below) or to your IBM Cloud dashboard. In either case, close your browser tab and proceed
to Exercise 2 below.

<img src="https://gitlab.com/SrishtiiSrivastava/markdown-project/-/raw/master/images/1.2.PNG"  width = 600>

# Exercise 2: Create a Watson Assistant service

Now that you have an IBM Cloud account, it's time to create an instance of IBM Watson
Assistant. Visit your IBM Cloud [dashboard](https://cocl.us/CB0103ENv2_dashboard) or simply type cloud.ibm.com in your browser’s
address bar. Next, follow these simple steps.

```
Note that you can always create new services (Watson Assistant or any other service available in
the IBM Cloud catalog) by clicking on the Create resource button on your dashboard.
```
1. **Click on the** **_Create resource_** **button** on your dashboard.
2. **Search for Watson Assistant** in the search field or find it in the AI category of the catalog and
    then **click on that tile**.

<img src="https://gitlab.com/SrishtiiSrivastava/markdown-project/-/raw/master/images/1.3.PNG"  width = 600>

3. You should see a Watson Assistant creation page similar to the image below.

<img src="https://gitlab.com/SrishtiiSrivastava/markdown-project/-/raw/master/images/1.4.PNG"  width = 600>

You can leave everything to default or customize the service instance name to your liking
(e.g., Watson Assistant Flower Shop). If you are not in the USA or Canada, you might want to
switch the region/location for optimal performance. It may take up to a minute before the UI
will allow you to select a different data center. Give it a moment to finish loading the page if
you don't see the option.

4. Take note of the differences between the free Lite plan (which you'll be using) and the other
    plans available, towards the end of the page.


5. With the Lite plan selected, **click on the** **_Create_** **button** at the bottom to create your instance.
6. You'll be redirected to the launch page for the service you just created. **Click on the** **_Launch_**
    **_Watson Assistant_** **button** to access the web application that will allow you to create chatbots.

<img src="https://gitlab.com/SrishtiiSrivastava/markdown-project/-/raw/master/images/1.5.PNG"  width = 600>

7. A default Assistant and Skill will be created for you as shown in the image below.

<img src="https://gitlab.com/SrishtiiSrivastava/markdown-project/-/raw/master/images/1.6.PNG"  width = 600>


You can think of the assistant as the actual chatbot. And a chatbot will have one or more skills.
Typically, a chatbot will have at least one dialog skill. In our case, My first assistant is our
chatbot, and My first skill is its dialog skill which enables it to hold conversations.


```
If My first assistant and My first skill were not created for you, and instead you see an error
pop up on the screen, clear your browser cache and log back in on cloud.ibm.com. From your
dashboard, click on Services , and then on the Watson Assistant instance you created (e.g.,
Watson Assistant Flower Shop ). Then repeat step 6.
```
8. Let’s rename our assistant. **Click on the three vertical dot icon** to the right of the page, above
    _Integrations_ and then **click on** **_Settings_**.

<img src="https://gitlab.com/SrishtiiSrivastava/markdown-project/-/raw/master/images/1.7.PNG"  width = 600>

Here, change the name of our assistant to Flower Shop Chatbot and optionally change the
description too.

<img src="https://gitlab.com/SrishtiiSrivastava/markdown-project/-/raw/master/images/1.8.PNG"  width = 600>

Once you’re done, click on the X at the top to return to our assistant.

9. While we are here, let’s rename its dialog skill as well. To do so, click on the Skills tab on the
top left, as shown below.

<img src="https://gitlab.com/SrishtiiSrivastava/markdown-project/-/raw/master/images/1.9.PNG"  width = 600>

Here, click on the vertical three dot icon next to My first skill, and then click on Rename as
shown below.

<img src="https://gitlab.com/SrishtiiSrivastava/markdown-project/-/raw/master/images/1.10.PNG"  width = 600>


**Change the name of this dialog skill to** Flower Shop Skill **and click** **_Rename_** **.** Optionally add a
description if you wish.

<img src="https://gitlab.com/SrishtiiSrivastava/markdown-project/-/raw/master/images/1.11.PNG"  width = 600>

Watson doesn’t really care about these labels and descriptions, but they help humans working
on the chatbot better understand how things are organized and why.

10. Now that you renamed the skill, we can go back to the assistant by clicking on the Assistants
tab as shown in the figure below.

<img src="https://gitlab.com/SrishtiiSrivastava/markdown-project/-/raw/master/images/1.12.PNG"  width = 600>

Notice how a list of assistants is shown. In our case there is only one, our _Flower Shop Chatbot_.
If you look closely, you’ll see that it includes our _Flower Shop Skill_ under its Skills section.


**Click on the tile containing our chatbot** (i.e., _Flower Shop Chatbot_ ). You’ll be sent back to the
familiar view we’ve encountered before, but this time with a properly named assistant.

<img src="https://gitlab.com/SrishtiiSrivastava/markdown-project/-/raw/master/images/1.13.PNG"  width = 600>

If this is what you see, we are all set for now. 

# Exercise 3: Opt out of the Preview

In order to improve its service, IBM runs so-called A/B tests where a portion of new users are shown
a newer, experimental user-interface. This is great for the product but can be quite confusing when
following along a course.

So, if you see the pink Preview icon at the top of your Watson Assistant interface, it means that you
are currently enrolled in the preview and need to opt out in order to have an easier time following
along the instructions in the next labs.

<img src="https://gitlab.com/SrishtiiSrivastava/markdown-project/-/raw/master/images/1.14.PNG"  width = 600>

If you don’t see the _Preview_ icon as shown in the figure above, you’re all done with this lab and
can proceed to the next section of this course.

If you see the _Preview_ icon, please follow these instructions to revert to the standard interface:

1. **Click on the account icon** in the top right of the page.
2. From the menu that appears, **click on the revert icon** next to your Watson Assistant instance
as shown in the figure below.

<img src="https://gitlab.com/SrishtiiSrivastava/markdown-project/-/raw/master/images/1.15.PNG"  width = 600>


3. Next, **click on the** **_Revert to standard_** **button** in the pop-up dialog that appears.

<img src="https://gitlab.com/SrishtiiSrivastava/markdown-project/-/raw/master/images/1.16.PNG"  width = 600>

This will revert your instance to a standard UI that will make your life a lot easier as you follow
along my course.
